import { Component, OnInit } from '@angular/core';
import { Pokemon } from '../interfaces/pokemon.interface';
import { PokeService } from '../services/poke.service';

@Component({
  selector: 'app-pokedex',
  templateUrl: './pokedex.component.html',
  styleUrls: [
    './pokedex.component.css',
    './pokedex-screen-up.css',
    './pokedex-tactil.css',
  ],
})
export class PokedexComponent implements OnInit {
  pokeballRotate: number = 0;

  get myPokemons(): Pokemon[] {
    return this.pokeService.myPokemons;
  }

  get pokeballRotateDeg(): string {
    return `transform: rotate(${this.pokeballRotate}deg)`;
  }

  constructor(private pokeService: PokeService) {
    this.pokeService.createMyPokemons();
  }

  ngOnInit(): void {}

  restartPokemons() {
    this.pokeService.restartNumber();
    this.pokeService.createMyPokemons();
  }

  rotatePokeballUp() {
    this.pokeballRotate += 30;
    this.pokeService.setNumber(-1);
    this.pokeService.createMyPokemons();
  }

  rotatePokeballDown() {
    this.pokeballRotate += -30;
    this.pokeService.setNumber(1);
    this.pokeService.createMyPokemons();
  }
}
