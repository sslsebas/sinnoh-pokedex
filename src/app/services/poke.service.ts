import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Pokemon } from '../interfaces/pokemon.interface';

@Injectable({
  providedIn: 'root',
})
export class PokeService {
  private _serviceURL: string = 'https://pokeapi.co/api/v2/pokemon/';
  private _response!: Pokemon;
  private _selectedNumber: number = -1;
  private _myPokemons: Pokemon[] = [];

  get myPokemons(): Pokemon[] {
    return [...this._myPokemons];
  }

  get selectedNumber(): number {
    return this._selectedNumber;
  }

  constructor(private http: HttpClient) {}

  setNumber(math: number) {
    this._selectedNumber += math;
  }

  restartNumber() {
    this._selectedNumber = -1;
  }

  /**
   * @description Get one Pokemon!
   * @param idPokemon Between 1 to 898
   */
  searchPokemon(idPokemon: number): Promise<Pokemon> {
    return this.http
      .get<Pokemon>(`${this._serviceURL}${idPokemon}`)
      .toPromise();
  }

  /**
   *
   * @returns a random number between 1 - 898
   */
  randomNumber(): number {
    let rand = Math.floor(Math.random() * 898);
    return rand;
  }

  /**
   * @description push 6 *new* Pokemons on public myPokemons
   */
  async createMyPokemons() {
    this._myPokemons = [];
    let number =
      this.selectedNumber === -1 ? this.randomNumber() : this.selectedNumber;
    this._selectedNumber = number;
    for (let index = 0; index < 7; index++) {
      let newPokemon: any = await this.searchPokemon(number + index);
      this._myPokemons.push(newPokemon);
    }
  }
}
